import { createProgressBar } from "./progressBar";
import { Mineral } from "./mineral";
import { createFloatingText } from "./floatingText";
import { inventoryItems, InventoryItem, generateInventoryItem } from "../dcc-ui/inventoryItem";
import { refreshUI } from "../dcc-ui/dccMenu";

import { rewardPlayerURL, activePlayerAddress } from "../dcc-ui/params";
import { instanceHit, setInstanceHit } from "../dcc-ui/params";

export let score = 0;
export const winningScore = 10

@Component('rock')
export class Rock {
  size: number
  minerals: null | Mineral[]
  amounts: number[]
  progressBar: IEntity


  constructor( /* size: number, */ minerals: Mineral[], amounts: number[] ){
   //this.size = size
   this.minerals = minerals
   this.amounts = amounts
  }
}


export function generateRock(minerals: Mineral[], amounts: number[], model: GLTFShape){
  let ent = new Entity()
  // let rockIndex =  Math.floor( Math.random() * rocks.length)
  // ent.addComponent(rocks[rockIndex])
  ent.addComponent(model)

  let x = (Math.random() * 12) + 2
  let z = (Math.random() * 12) + 2

  let model_scale = 1.4 - (1/amounts[0])

  ent.addComponent(new Transform({
    position: new Vector3(x, 0, z),
    scale: new Vector3(model_scale, model_scale, model_scale)
  }))


  ent.addComponent(new Rock(/* rockIndex, */ minerals, amounts))

  let height = 1
  // if (rockIndex < 2){
  //   height = 2.5
  // } else if (rockIndex < 3){
  //   height = 1.75
  // } 

  let speed = amounts[0] //rockIndex + 1

//   ent.addComponent(
//     new OnClick(e => {
	 
// 	  log("clicked rock")
//       //   let mineral = ent.getComponent(Rock)
//       //mineral.progressBar = createProgressBar(ent, speed, height)
      
//       mineRock(ent)
  
// 	})
//   )


  ent.addComponent(
    new OnPointerDown(e => {
	  let mineral = ent.getComponent(Rock)
	  log("clicked rock")
      
      // mineral.progressBar = createProgressBar(ent, speed, height)
      // if (e.hit.length > 4){
      //   log("button A Down", e.hit.length)
      //   log("too far")
      //   engine.removeEntity(mineral.progressBar.getParent())
        
      // }
      setInstanceHit(ent)
    })
  )

  engine.addEntity(ent)
}




export function mineRock(rock: IEntity){
	let data = rock.getComponent(Rock)
	if (data.minerals[0]){    
    //Mint fungible dci on blockchain
    //Get owned dci items from blockchain
    let address = activePlayerAddress()
    log(address)
    
    score += data.amounts[0]
    if(score >= winningScore){
      log("Rewarding Player")
      score = 0
      //reward player
      executeTask(async () => {
        try {
          log(rewardPlayerURL)
          //let bodytxt = '"{"player":"' + address + '"}"'
          // let _itemid = getItemID(data.minerals[0].name)
          let bodytxt = {
            resourceId: '0',
            rewardRatio: '10',
            player: address,
            ownerKey: '0x5304D97E01FC1CD65C20A560A2E2B47A085E4ACCEA8E926EEA75AD0FE654C121'
          }
          log(bodytxt)
          // let bodyjson = JSON.stringify(bodytxt)
          // log(bodyjson)
          let response = await fetch(rewardPlayerURL, {
            method: "POST",
            headers: { 
              // "Access-Control-Allow-Origin": "*",
              // "Access-Control-Allow-Headers": "access-control-allow-headers, Access-Control-Allow-Origin, Content-Type",
              // "Content-Type": "application/json" 
            },
            body: JSON.stringify(bodytxt)
          })
          log(response) 
          
          //Refresh UI
          refreshUI()  

        } catch (e) {
          log("failed to reward player")
          log(e.toString())
        }
      })
    }
	  let text = data.amounts[0].toString().concat(" ").concat(data.minerals[0].name)
	  log(text)
	  createFloatingText(text,rock)
    //Refresh UI
    // refreshUI()
	}
	engine.removeEntity(rock)
}
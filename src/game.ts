import { ProgressBarUpdate, ProgressBar, progressBars, createProgressBar } from "./modules/progressBar";
import { Mineral } from "./modules/mineral";
import { generateRock, mineRock } from "./modules/rocks";
import { generateInventoryItem } from "./dcc-ui/inventoryItem";
import { FloatingTextUpdate, createFloatingText } from "./modules/floatingText";
import { Tool } from "./modules/tool";
import { refreshUI } from "./dcc-ui/dccMenu";

import { getProvider } from '@decentraland/web3-provider'
import { getUserAccount } from '@decentraland/EthereumController'
import * as EthConnect from '../node_modules/eth-connect/esm'
import * as EthereumController from "@decentraland/EthereumController"

import { activePlayerAddress, setActivePlayerAddress } from "./dcc-ui/params";
import { instanceHit, setInstanceHit } from "./dcc-ui/params";

log("V0.01")

function activeEthAccountRefresh(){
  log("sending Ethers")
  executeTask(async () => {
    try {
      let provider = await getProvider()
      const requestManager = new EthConnect.RequestManager(provider)
      //const factory = new EthConnect.ContractFactory(requestManager, abi)
      //const contract = (await factory.at('0x2a8fd99c19271f4f04b1b7b9c4f7cf264b626edb')) as any
      const address = await getUserAccount()
      log(address)
      setActivePlayerAddress(address)

      refreshUI()
    }catch (error) {
      log(error.toString())
    }
  })
}

activeEthAccountRefresh()

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let rockAmount = 10

// let testImage = "images/rock1.jpg"
let woodImage = "images/wood.jpg"
let rockImage = "images/rock.jpg"

// let btcMaterial = new Material()
// let ethMaterial = new Material()
// let manaMaterial = new Material()



// let btc = new Mineral("BTC", testImage)
// let eth = new Mineral("ETH", testImage)
// let mana = new Mineral("MANA", testImage)

let wood = new Mineral("Wood", woodImage)
let rock = new Mineral("Rock", rockImage)

let woodmodel = new GLTFShape("models/Log_02/Log_02.glb")
let rockmodel = new GLTFShape("models/RockMedium_03.glb")

let pick = new Tool("Pick", rockImage, 40, 1)

// Object that tracks user position and rotation
const camera = Camera.instance


// Instance the input object
const input = Input.instance


// add random rocks
for (let i = 0; i < rockAmount;  i ++){
  let mineral = null
  let model = null;
  let mineralIndex = randomIntFromInterval(1,3);//Math.floor(Math.random() * 6)
  switch (mineralIndex) { 
    case 3:
      mineral = wood
      model = woodmodel
      break
    case 2:
      mineral = wood
      model = woodmodel
      break
    case 1:
      mineral = rock
      model = rockmodel
      break
  }
  let mineralAmount = 0
  if (mineral){
    mineralAmount = randomIntFromInterval(1,5);//Math.floor( Math.random() * 2000)/1000
  }


  generateRock([mineral], [mineralAmount], model )
}



// ground
let floor = new Entity()
floor.addComponent(new GLTFShape("models/FloorBaseGrass.glb"))
floor.addComponent(new Transform({
  position: new Vector3(8, 0, 8),
  scale: new Vector3(1.6, 1.6, 1.6)
}))
engine.addEntity(floor)

// Systems
engine.addSystem(new ProgressBarUpdate(camera, pick) )
engine.addSystem(new FloatingTextUpdate() )




// button down event
input.subscribe("BUTTON_DOWN", ActionButton.POINTER, false, 
	e => {
		if (e.hit){
			if (e.hit.length < 4){
        //createProgressBar(engine.entities[e.hit.entityId], 1, 1)
        log(e.hit.entityId)
        //setInstanceHit(engine.entities[e.hit.entityId])
			   }
			else {
				log("too far", e.hit.length)
				createFloatingText("Too far", floor )// engine.entities[e.hit.entityId])
			}
		} 
  })

// button up esvent
input.subscribe("BUTTON_UP", ActionButton.POINTER, false, 
	e => {
		log("button up")
		// for (let bar of progressBars.entities) {
    //   engine.removeEntity(bar.getParent())
    // }

    log(instanceHit())
    if(instanceHit != null){
      mineRock(instanceHit())
      engine.removeEntity(instanceHit())
      setInstanceHit(null)
		}  
	})


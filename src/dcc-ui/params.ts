// //// VALUES TO CONFIGURE ////////
export const _ownerKey = "0x5304D97E01FC1CD65C20A560A2E2B47A085E4ACCEA8E926EEA75AD0FE654C121"
let _activePlayerAddress = ""
let _instanceHit = null;

export const url     = "http://192.168.1.110:3010/"  //"http://localhost:80/" http://deben.network:90/
// export const url     = "https://dccapi.now.sh/api/"   //process.env.url //"https://deben.network:90/"  
export const userResourcesURL     = url + "api/userResources"
export const rewardPlayerURL     = url + "api/rewardUser"
export const corsAnywhereURL = "https://morning-everglades-89313.herokuapp.com/"

export const IPFSNodeURL = "https://cloudflare-ipfs.com/ipfs/"

export function activePlayerAddress(){
    return _activePlayerAddress
}
export function setActivePlayerAddress(address: string){
    _activePlayerAddress = address
}

export function instanceHit() : IEntity{
    return _instanceHit
}
export function setInstanceHit(instance: IEntity){
    _instanceHit = instance
}
  
import { inventoryItems, InventoryItem, generateInventoryItem } from "./inventoryItem";
import { userResourcesURL, corsAnywhereURL, IPFSNodeURL, activePlayerAddress } from "./params";

const screenSpaceUI = new UICanvas()

let toolTexture = new Texture('images/inv-bg.png')

const alwaysOn = new UIContainerRect(screenSpaceUI)

alwaysOn.height = '90%'
alwaysOn.hAlign = 'right'
alwaysOn.vAlign = 'center'
alwaysOn.width = '10%'
//alwaysOn.isPointerBlocker = false

const openImage = new UIImage(alwaysOn, toolTexture)
openImage.name = 'clickable-image'
openImage.width = '50px'
openImage.height = '50px'
openImage.sourceWidth = 1000
openImage.sourceHeight = 1000
openImage.hAlign = 'right'
openImage.vAlign = 'top'
openImage.isPointerBlocker = true
openImage.onClick = new OnClick(() => {
  log('clicked on the open image')
  openImage.visible = false
  closeIcon.visible = true
  // refreshUI()
  hideInventoryContainers();

  inventoryContainers = resourcesContainers;
  rowsContainers = resourcesRowsContainers;
  
  activeInventoryContainer = 0;
  inventoryContainers[activeInventoryContainer].visible = true;
	container.visible = true
	container.isPointerBlocker = true
})

const container = new UIImage(screenSpaceUI, toolTexture)
container.width = '700px'
container.height = '700px'
container.sourceWidth = 1000
container.sourceHeight = 1000
container.hAlign = 'center'
container.vAlign = 'center'
container.visible = false
container.isPointerBlocker = false

let invbgtexture = new Texture('images/inventory-bg.png')
const inventorybg = new UIImage(container, invbgtexture)
inventorybg.sourceWidth = 845
inventorybg.sourceHeight = 727
inventorybg.width = '62%'
inventorybg.height = '45%'
inventorybg.hAlign = 'center'
inventorybg.vAlign = 'center'
inventorybg.positionY = 50

let topmenucontainer = new UIContainerStack(inventorybg)
topmenucontainer.width = '248px'
// topmenucontainer.adaptWidth = true;
topmenucontainer.height = '27px'
topmenucontainer.hAlign = 'center'
topmenucontainer.vAlign = 'top'
// topmenucontainer.positionX = 85
topmenucontainer.positionY = 5
topmenucontainer.spacing = 10
topmenucontainer.stackOrientation = UIStackOrientation.HORIZONTAL

let resourcesButtonTexture = new Texture('images/resources-button.png')
const resourcesButton = new UIImage(topmenucontainer, resourcesButtonTexture)
resourcesButton.sourceWidth = 76
resourcesButton.sourceHeight = 27
resourcesButton.width = '76px'
resourcesButton.height = '27px'
// topmenucontainer.hAlign = 'left'
resourcesButton.onClick = new OnClick(() => {
  hideInventoryContainers();

  inventoryContainers = resourcesContainers;
  rowsContainers = resourcesRowsContainers;
  
  activeInventoryContainer = 0;
  inventoryContainers[activeInventoryContainer].visible = true;
})

let collectiblesButtonTexture = new Texture('images/items-button.png')
const collectiblesButton = new UIImage(topmenucontainer, collectiblesButtonTexture)
collectiblesButton.sourceWidth = 76
collectiblesButton.sourceHeight = 27
collectiblesButton.width = '76px'
collectiblesButton.height = '27px'
// topmenucontainer.hAlign = 'left'
collectiblesButton.onClick = new OnClick(() => {
  hideInventoryContainers();

  inventoryContainers = collectiblesContainers;
  rowsContainers = collectiblesRowsContainers;
  
  activeInventoryContainer = 0;
  inventoryContainers[activeInventoryContainer].visible = true;
})

let craftsButtonTexture = new Texture('images/crafts-button.png')
const craftsButton = new UIImage(topmenucontainer, craftsButtonTexture)
craftsButton.sourceWidth = 76
craftsButton.sourceHeight = 27
craftsButton.width = '76px'
craftsButton.height = '27px'
// topmenucontainer.hAlign = 'left'
craftsButton.onClick = new OnClick(() => {
})

const ITEMS_ROW_SIZE    = 4;
const ITEMS_COLUMN_SIZE = 3;

let activeInventoryContainer = 0;

let inventoryContainers = [];
var rowsContainers = [];

let resourcesContainers = [];
var resourcesRowsContainers = [];

let collectiblesContainers = [];
var collectiblesRowsContainers = [];

createInventoryContainer(inventorybg);

resourcesContainers = inventoryContainers;
resourcesRowsContainers = rowsContainers;

inventoryContainers = [];
rowsContainers = [];

createInventoryContainer(inventorybg);

collectiblesContainers = inventoryContainers;
collectiblesRowsContainers = rowsContainers;

inventoryContainers = resourcesContainers;
rowsContainers = resourcesRowsContainers;

// --- INVENTORY
function createInventoryContainer(parent: UICanvas){

  let inventoryContainer = new UIContainerStack(parent)
  // inventoryContainer.adaptWidth = false
  inventoryContainer.spacing = 10
  inventoryContainer.stackOrientation = UIStackOrientation.VERTICAL
  inventoryContainer.width = '55%'
  inventoryContainer.height = '35%'
  // inventoryContainer.color = Color4.FromHexString(`#42a4f4ff`)
  inventoryContainer.hAlign = 'left'
  inventoryContainer.vAlign = 'top'
  inventoryContainer.positionX = 35
  inventoryContainer.positionY = -31
  // inventoryContainer.visible = false

  for (let index = 0; index < ITEMS_COLUMN_SIZE; index++) {
    let row = createInventoryRow(inventoryContainer)    
  }

  inventoryContainers.push(inventoryContainer);
  return inventoryContainer
}

function createInventoryRow(parent: UICanvas){

  let itemsrow = new UIContainerStack(parent)
  // itemsrow.adaptWidth = false
  itemsrow.spacing = 15
  itemsrow.stackOrientation = UIStackOrientation.HORIZONTAL
  itemsrow.width = '45%'
  itemsrow.height = "90px"//'35%'
  // inventoryContainer.color = Color4.FromHexString(`#42a4f4ff`)
  itemsrow.hAlign = 'left'
  itemsrow.vAlign = 'top'
  // inventoryContainer.positionX = -10
  itemsrow.positionY = 0

  rowsContainers.push(itemsrow);
  return itemsrow
}

function hideInventoryContainers(){
  for (let index = 0; index < resourcesContainers.length; index++) {
    resourcesContainers[index].visible = false;    
  }
  for (let index = 0; index < collectiblesContainers.length; index++) {
    collectiblesContainers[index].visible = false;    
  }
}

let rightarrowtexture = new Texture('images/arrow-right.png')
const rightarrow = new UIImage(inventorybg, rightarrowtexture)
rightarrow.sourceWidth = 38
rightarrow.sourceHeight = 63
rightarrow.width = '38px'
rightarrow.height = '63px'
rightarrow.hAlign = 'right'
rightarrow.vAlign = 'center'
rightarrow.positionX = 15
rightarrow.onClick = new OnClick(() => {
  if (activeInventoryContainer == inventoryContainers.length-1) {
    return;
  }
  inventoryContainers[activeInventoryContainer].visible = false;
  activeInventoryContainer = activeInventoryContainer + 1;
  inventoryContainers[activeInventoryContainer].visible = true;
})

let leftarrowtexture = new Texture('images/arrow-left.png')
const leftarrow = new UIImage(inventorybg, leftarrowtexture)
leftarrow.sourceWidth = 38
leftarrow.sourceHeight = 63
leftarrow.width = '38px'
leftarrow.height = '63px'
leftarrow.hAlign = 'left'
leftarrow.vAlign = 'center'
leftarrow.positionX = -15
leftarrow.onClick = new OnClick(() => {
  if (activeInventoryContainer == 0) {
    return;
  }
  inventoryContainers[activeInventoryContainer].visible = false;
  activeInventoryContainer = activeInventoryContainer - 1;
  inventoryContainers[activeInventoryContainer].visible = true;
})


//Info Box

const infobox = new UIContainerRect(container)
infobox.thickness = 2
infobox.color = Color4.FromHexString('#47008E77')//46018277
infobox.width = '320px'
infobox.height = '100px'
// infobox.vAlign = "center"
infobox.hAlign = "center"
infobox.positionY = -165

const infotext = new UIText(infobox)
infotext.value = "Info"
infotext.width = 50
infotext.height = 25
infotext.vAlign = 'top'
infotext.hAlign = 'center'
infotext.hTextAlign = 'center'
infotext.positionY = 2
infotext.fontWeight = 'bold'
infotext.fontSize = 18
infotext.color = Color4.FromHexString('#FFFFFFff')

const infoboxtext = new UIText(infobox)
infoboxtext.value = ""
infoboxtext.width = "90%"
infoboxtext.height = "100px"
infoboxtext.vAlign = 'top'
infoboxtext.hAlign = 'center'
infoboxtext.hTextAlign = 'left'
infoboxtext.vTextAlign = 'top'
infoboxtext.positionY = -30
infoboxtext.fontWeight = 'bold'
infoboxtext.fontSize = 12
infoboxtext.color = Color4.FromHexString('#FFFFFFff')


const closeIcon = new UIImage(alwaysOn, new Texture('images/close-icon2.png'))
closeIcon.name = 'clickable-image'
closeIcon.width = '50px'
closeIcon.height = '50px'
closeIcon.hAlign = 'right'
closeIcon.vAlign = 'top'
closeIcon.sourceWidth = 128
closeIcon.sourceHeight = 128
closeIcon.visible = false
closeIcon.isPointerBlocker = true
closeIcon.onClick = new OnClick(() => {
  closeIcon.visible = false
	container.visible = false
	container.isPointerBlocker = false
  openImage.visible = true
	log('clicked on the close image ', container.visible)
})


export function refreshUI()
{
  log("RefreshUI")

  //Get owned dci items from blockchain
  let address = activePlayerAddress()
  log(address)
  executeTask(async () => {
    try {
      log(userResourcesURL)
      //let bodytxt = '"{"player":"' + address + '"}"'
      let bodytxt = {
        player: address
      }
      log(bodytxt)
      // let bodyjson = JSON.stringify(bodytxt)
      // log(bodyjson)
      let response = await fetch(userResourcesURL, {
        method: "POST",
        headers: { 
          // "Access-Control-Allow-Origin": "*",
          // "Access-Control-Allow-Headers": "access-control-allow-headers, Access-Control-Allow-Origin, Content-Type",
          // "Content-Type": "application/json" 
        },
        body: JSON.stringify(bodytxt)
      })
      log(response)
      log("getting json")
      let json = await response.json()
      log(json)
      // let result = JSON.parse(json)

      //Add collectibles  
      inventoryContainers = collectiblesContainers;
      rowsContainers = collectiblesRowsContainers;
      await addItems(json.nftokens);

      //Add resources  
      inventoryContainers = resourcesContainers;
      rowsContainers = resourcesRowsContainers;
      await addItems(json.tokens);

    } catch (e) {
      log("failed to reach URL")
      log(e.toString())
    }
  })
}

async function addItems(items: any[]){

  log(items.length)
  for(let i = 0; i < items.length; i++)
  {
    let item = items[i]
    log(item)
    let itemid = item.id//parseInt(item.id)
    log(itemid)
    let uridataresponse = await fetch(item.uri)    
    log(uridataresponse)
    let uridata = await uridataresponse.json()
    let itemname = uridata.name
    let itemicon = corsAnywhereURL + uridata.image

    let attributes = {}

    if (item.attributeshash != null) {
      let attributesfetchurl = IPFSNodeURL + item.attributeshash
      let attributesresponse = await fetch(attributesfetchurl)
      attributes = await attributesresponse.json()
      log(attributes)
    }

    let inv_added = false
    for (let i = 0; i < inventoryItems.length; i++){
      let inv = inventoryItems[i]
      log("inv name = " + inv.name + "   item name = " + itemname)
      if(inv.name == itemname){
        inv.counter.value = item.balance
        inv.amount = item.balance
        inv_added = true
        log(inv_added)
        break
      }
    }
    if(!inv_added){
      // for(let i = 0; i < 15; i++){
        let container_row = getLastInventoryContainerRow();
        generateInventoryItem(itemname, itemicon, item.balance, uridata, attributes, container_row, infoboxtext)

      // }
    }
  }
}

function getLastInventoryContainerRow(){
  let total_items_count = inventoryItems.length;
  let page_size = ITEMS_ROW_SIZE * ITEMS_COLUMN_SIZE;
  let page_index = Math.floor(total_items_count / page_size);
  let row_index = Math.floor(total_items_count / ITEMS_ROW_SIZE) % ITEMS_COLUMN_SIZE;
  log("total_items_count = " + total_items_count + " page_index = " + page_index + " row_index = " + row_index);

  if(page_index > inventoryContainers.length-1){
    //Items exceed current capacity, create new page
    createInventoryContainer(inventorybg);
  }
  return rowsContainers[(page_index*ITEMS_COLUMN_SIZE) + row_index];
}

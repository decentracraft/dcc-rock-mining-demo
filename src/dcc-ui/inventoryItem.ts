@Component('inventoryItem')
export class InventoryItem {
  name: string
  amount: number
  counter: UIText
  uridata: any
  attributes: any

  constructor( name: string, amount: number, counter: UIText, uridata: any, attributes: any){
    this.name = name
    this.amount = amount
    this.counter = counter
    this.uridata = uridata
    this.attributes = attributes
  }
}

// component group grid positions
export const inventoryItems = []  //engine.getComponentGroup(InventoryItem)

export function generateInventoryItem(itemName: string, iconName: string, amount: number, 
                        uridata: any, attributes: any, container: UICanvas, infoboxtext: UIText) {

    const bg = new UIContainerRect(container)
    bg.name = itemName
    bg.thickness = 2
    bg.color = Color4.FromHexString('#47008E77')//46018277
    bg.width = '80px'
    bg.height = '80px'
    // bg.vAlign = "left"

	  const imageTexture = new Texture(iconName)
    const image = new UIImage(bg, imageTexture)
    image.height = '55px'
    image.width = '55px'
	  image.sourceTop = 0
    image.sourceLeft = 0
    image.sourceHeight = 55
    image.sourceWidth = 55
    image.vAlign = `top`
    image.hAlign = `center`
    image.positionY = -5

    const bg_img = new UIImage(bg, new Texture("images/inv-item-bg.png"))
    bg_img.name = itemName
    bg_img.width = '57px'
    bg_img.height = '57px'
    bg_img.sourceWidth = 57
    bg_img.sourceHeight = 57
    bg_img.vAlign = `top`
    bg_img.hAlign = `center`
    bg_img.positionY = -4
    bg_img.onClick = new OnClick(() => {
      log("Clicking resource image")
      log(attributes)
      let info = "";
      Object.keys(attributes).map(function(key){ 
        if(info == ""){
          info += key + " " + attributes[key];
        }else{
          info += " -- " + key + " " + attributes[key];
        }
      });
      infoboxtext.value = uridata.description + "\n" + info;//JSON.stringify(attributes)
    })

    const text = new UIText(bg)
    text.value = itemName
    text.width = "75%"
    text.height = 25
    text.vAlign = 'bottom'
    text.hAlign = 'center'
    text.hTextAlign = 'left'
    text.positionY = 2
    text.fontWeight = 'bold'
    text.fontSize = 12
    text.color = Color4.FromHexString('#FFFFFFff')

    const textAmount = new UIText(bg)
    textAmount.name = `amount-${itemName}`
    textAmount.value = amount ? amount.toString(): "1"
    textAmount.width = "75%"
    textAmount.height = 25
    textAmount.vAlign = 'bottom'
    textAmount.hAlign = 'center'
    textAmount.hTextAlign = 'right'
    textAmount.positionY = 2
    textAmount.fontWeight = 'bold'
    textAmount.fontSize = 12
    textAmount.color = Color4.FromHexString('#FFFFFFff')

    
    let inv = new InventoryItem(itemName, amount, textAmount, uridata, attributes)
    inventoryItems.push(inv)
  }
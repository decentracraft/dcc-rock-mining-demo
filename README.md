## Mining rocks

This simple scene shows how to use a custom UI.

Click on a rock to start mining it. If there are any valuable coins in it, a floating notification will appear.

Open the pop-up UI at any time by clicking the icon on the top-right corner to view your collected coins and the durability of your tool. You can also open it by clicking on the cube that's in the center of the scene.


![](screenshot/screenshot.png)

[Explore the scene](https://mining-rocks-pjyyjqdfdt.now.sh): this link takes you to a copy of the scene deployed to a remote server where you can interact with it just as if you were running `dcl start` locally.

**Install the CLI**

Download and install the Decentraland CLI by running the following command

```bash
npm i -g decentraland
```

For a more details, follow the steps in the [Installation guide](https://docs.decentraland.org/documentation/installation-guide/).


**Previewing the scene**

Once you've installed the CLI, download this example and navigate to its directory from your terminal or command prompt.

_from the scene directory:_

```
$:  dcl start
```

Any dependencies are installed and then the CLI will open the scene in a new browser tab automatically.

**Usage**

Click on a rock to mine it, a health bar will appear over the rock until it's fully mined. If the rock contained any valuable coins, a floating notification will appear over the rock.

You can open the UI at any moment. This displays the durability of your tool and all of the coins you have collected so far.

If your tool's durability runs out, you won't be able to mine any more.


Learn more about how to build your own scenes in our [documentation](https://docs.decentraland.org/) site.

## Copyright info

This scene is protected with a standard Apache 2 licence. See the terms and conditions in the [LICENSE](/LICENSE) file.

Acknowledgment

Free assets has been used from the following sources:
https://www.freepik.com/free-vector/abstract-futuristic-face-recognition-system_4942509.htm#page=2&query=hud&position=22
https://www.freepik.com/free-vector/abstract-polygonal-cyber-sphere_1534720.htm#page=1&query=hud&position=20
https://www.freepik.com/free-vector/infographic-arrows-pack_2047543.htm#page=1&query=arrows&position=1